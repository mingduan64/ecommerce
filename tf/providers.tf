# Configure the AWS provider
provider "aws" {
  region = "us-east-1" # Replace with your desired AWS region
  # You can also specify other AWS provider settings here, such as access_key and secret_key if needed
}
