resource "aws_ecr_repository" "example" {
  count = length(var.ecr_repository_names)

  name = var.ecr_repository_names[count.index]
  # other ECR repository configuration
}


