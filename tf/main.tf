terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
  }
}

# Include the ECR module for Docker image registry
module "ecr" {
  source = "./ecr" # Replace with the path with your ECR module

  # Define input variables for the ECR module
  # e.g., ecr_repository_name = "my-app-repo"
}

# Include the EKS module for the Kubernetes cluster
module "eks" {
  source = "./eks" # Replace with the path to your EKS module

  # Define input variables for the EKS module
  # e.g., eks_cluster_name = "my-cluster"
}

# Include the network module for VPC, subnets, etc.
module "network" {
  source = "./network" # Replace with the path to your network module

  # Define input variables for the network module
  # e.g., vpc_cidr_block = "10.0.0.0/16"
}

# Include the security module for IAM roles, security groups, etc.
module "security" {
  source = "./security" # Replace with the path to your security module

  # Define input variables for the security module
  # e.g., iam_role_name = "my-app-role"
}

# Include the monitoring module for CloudWatch, Container Insights, etc.
module "monitoring" {
  source = "./monitoring" # Replace with the path to your monitoring module

  # Define input variables for the monitoring module
  # e.g., log_group_name = "/ecs/my-app-task-logs"
}
