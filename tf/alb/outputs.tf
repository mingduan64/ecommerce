output "alb_dns_name" {
  description = "The DNS name of the Application Load Balancer"
  value       = aws_lb.example.dns_name
}

output "alb_arn" {
  description = "The ARN (Amazon Resource Name) of the Application Load Balancer"
  value       = aws_lb.example.arn
}

output "alb_security_group_ids" {
  description = "A list of security group IDs associated with the Application Load Balancer"
  value       = aws_lb.example.security_groups
}
