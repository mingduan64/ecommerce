provider "null" {
}

resource "null_resource" "add_update_helm_repo" {
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    # Add Helm repository
    command = "helm repo add eks https://aws.github.io/eks-charts"
  }

  provisioner "local-exec" {
    # Update Helm repositories
    command = "helm repo update"
  }
}

resource "null_resource" "install_aws_load_balancer_controller" {
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    # Install AWS Load Balancer Controller using Helm
    command = <<-EOT
      helm install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=ex-eks --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller --set region=ap-southeast-1 --set vpcId=vpc-04bb017124b463e4b --set app.kubernetes.io/managed-by=Helm --set meta.helm.sh/release-name=aws-load-balancer-controller --set meta.helm.sh/release-namespace=kube-system
    EOT
  }
}
