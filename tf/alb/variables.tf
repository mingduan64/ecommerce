variable "vpc_id" {
  description = "The ID of the VPC where the ALB should be created."
  type        = string
}

variable "subnet_ids" {
  description = "A list of subnet IDs where the ALB should be deployed."
  type        = list(string)
}

variable "alb_name" {
  description = "The name of the Application Load Balancer."
  type        = string
  default     = "my-alb"
}
