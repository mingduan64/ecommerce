# Define the AWS provider and region
provider "aws" {
  region = "us-east-1" # Replace with your desired AWS region
}

# Define an Amazon CloudWatch log group for Fargate logs
resource "aws_cloudwatch_log_group" "fargate_logs" {
  name              = "/ecs/my-fargate-task"
  retention_in_days = 30 # Adjust retention policy as needed
}

# Create an Amazon CloudWatch Container Insights configuration for Fargate
resource "aws_cloudwatch_container_insight" "fargate_insight" {
  name     = "my-fargate-insight"
  container_insights_version = "LATEST"
  cloudwatch_container_insights_enabled = true
}
