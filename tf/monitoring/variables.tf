variable "log_group_name" {
  description = "The name of the CloudWatch log group for Fargate logs."
  type        = string
  default     = "/ecs/my-fargate-task"
}

variable "log_retention_days" {
  description = "The retention period in days for CloudWatch logs."
  type        = number
  default     = 30
}

variable "container_insights_name" {
  description = "The name of the CloudWatch Container Insights configuration."
  type        = string
  default     = "my-fargate-insight"
}

variable "container_insights_enabled" {
  description = "Whether to enable CloudWatch Container Insights."
  type        = bool
  default     = true
}
