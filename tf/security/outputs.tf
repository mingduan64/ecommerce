output "security_group_id" {
  description = "ID of the created security group"
  value       = aws_security_group.example.id
}

output "security_group_name" {
  description = "Name of the created security group"
  value       = aws_security_group.example.name
}

# Add more outputs as needed
